### Welcome to the Zeus IRC Daemon [Visit us!](http://zeusircd.net)

~~~
 ZeusIRCD is an IRC Daemon writed in C++ Lang with
 support to use SQLite3 & MySQL API Engine and Multi-Lingual
 and an advanced asyncronous engine boost based. Try it !!!
~~~

__HOW TO INSTALL__ [Click Here](https://github.com/Pryancito/zeusircd/wiki/INSTALLATION-GUIDE)

### Acknowledgment

- [blacknode](https://github.com/blacknode/)
- [Pryancito](https://github.com/Pryancito/)
- [Zoltan](https://github.com/zoltyvigo/)
- [Damon](https://github.com/D4M0N1979/)
- PaCMaN
